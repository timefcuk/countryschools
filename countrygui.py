# -*- coding: utf-8 -*-
from Tkinter import *
import numpy as np
import transport as tt
import countrydb as cdb

#COLORS
framecolor="#FFFFFF"
redbutton="#AAF0D9" #I am not red anymore, but historically I still have this name.
greenbutton="#C0FFC0"
nextbutton="#98FB98"
schoolcolor="#FFFACD"
nicecolor="#FFFF9E" # Don't delete me, I am very nice color :) -> "#7FFFD4"

#FONTS
headerfont = ("Times", 20)
italicfont = ("Times", 15, "italic")
mainfont = ("Times", 15)
buttonfont = ("Times", 16, "bold")
stepsfont = ("Times", 16)

class GreetingWindow:
    def __init__(self):
        self.master = Tk()
        height=510
        width=1200
        #Getting screen width and screen height
        ws=self.master.winfo_screenwidth()
	hs=self.master.winfo_screenheight()
        #Calculating x and y coordinates for Tk master window
        x = (ws/2) - (width/2)
        y = (hs/2) - (height/2)
        #Setting the dimensions of the screen and where it is placed
        self.master.geometry('%dx%d+%d+%d' % (width, height, x, y))
        self.master.configure(bg=framecolor)
        self.master.title(u"Описание этапов работы с программой")
        
        Label(self.master, 
              text=u"Этапы распределения нагрузки преподавателей по школам в рамках одной дисциплины:", 
              height=2, 
              bg=framecolor, 
              font=headerfont).grid(row=0 , sticky=W+E)

        Label(self.master, 
              text=u"            Шаг 1:", 
              bg=framecolor, 
              font=stepsfont).grid(row=1, sticky=W)

        Label(self.master, 
              text=u"            Выбор дисциплины из списка или добавление новой.",
              height=2, 
              bg=framecolor, 
              font=mainfont).grid(row=2, sticky=W)

        Label(self.master, 
              text=u"            Шаг 2:", 
              bg=framecolor, 
              font=stepsfont).grid(row=3, sticky=W)

        Label(self.master, 
              text=u"            Добавление всех школ с вакансией по выбранной дисциплине.", 
              height=2, 
              bg=framecolor, 
              font=mainfont).grid(row=4, sticky=W)

        Label(self.master, 
              text=u"            Шаг 3:", 
              bg=framecolor, 
              font=stepsfont).grid(row=5, sticky=W)

        Label(self.master, 
              text=u"            Добавление в список всех учителей по этой дисциплине с недостатком рабочей нагрузки. (Дополнительно) В случае добавления\nв список новой школы и/или учителя необходимо ввести расстояние маршрута от каждого учителя до каждой из школ.", 
              height=2, 
              bg=framecolor, 
              font=mainfont).grid(row=6, sticky=W)
#        Label(self.master, text=u"            Шаг 3.1:", bg=framecolor, font=stepsfont).grid(row=7, sticky=W)
#        Label(self.master, text=u"            В случае добавления новой школы или учителя необходимо ввести расстояние маршрута от учителя до каждой из школ.", height=2, bg=framecolor, font=mainfont).grid(row=8, sticky=W)
        Label(self.master, 
              text=u"            Шаг 4:", 
              bg=framecolor, 
              font=stepsfont).grid(row=9, sticky=W)

        Label(self.master, 
              text=u"            Просмотр результатов распределения учителей определённой дисциплины по школам.", 
              height=2, 
              bg=framecolor, 
              font=mainfont).grid(row=10, sticky=W)

        Button( self.master, 
                text=u'Перейти к Шагу 1: Выбор дисциплины ->', 
                command=self.nextStep, 
                width=107, 
                height=3, 
                bg=greenbutton, 
                font=buttonfont, 
                highlightbackground="#B0C4DE", 
                activebackground=nicecolor).grid(row=11, sticky=W+E)

        mainloop()

    def nextStep(self):
        self.master.destroy()
        nw = SpecGUI()

class SpecGUI:
    def __init__(self):
        self.master = Tk()
        height=600
        width=600
        #Getting screen width and screen height
        ws=self.master.winfo_screenwidth()
	hs=self.master.winfo_screenheight()
        #Calculating x and y coordinates for Tk master window
        x = (ws/2) - (width/2)
        y = (hs/2) - (height/2)
        #Setting the dimensions of the screen and where it is placed
        self.master.geometry('%dx%d+%d+%d' % (width, height, x, y))
        self.master.configure(bg=framecolor)
        self.master.title(u"Выбор дисциплины")
        self.database_teleport = cdb.Countrydb()
    
        Label(self.master, text=u"Шаг 1", bg=framecolor, font=headerfont).pack(fill=X)
        Label(  self.master, 
                text = u"Выберите дисциплину:",
				bg=framecolor,
				font=headerfont).pack(fill=X)
        
        self.SubjectList = Listbox(self.master, font=headerfont, selectbackground=nicecolor)
        self.SubjectList.pack(fill=X)
        
        setofbuttons=Frame(height=200,width=300)

        Button( setofbuttons, 
                text=u'Добавить',
                command=self.addSubj,
				height=2,
				width=25,
				bg=redbutton,
				font=buttonfont,
				highlightbackground="#B0C4DE",
				activebackground=nicecolor).pack(side=LEFT)

        Button( setofbuttons, 
                text=u'Удалить',
                command=self.delSubj,
				height=2,
				width=25,
				bg=redbutton,
				font=buttonfont, 
				highlightbackground="#B0C4DE",
				activebackground=nicecolor).pack(side=LEFT)

        setofbuttons.pack()

        Button( self.master, 
                text=u'Перейти к Шагу 2: Добавление школ ->',
                command=self.nextWindow,
				height=3,
				width=30,
				bg=greenbutton,
				font=buttonfont,
				highlightbackground="#B0C4DE",
				activebackground=nicecolor).pack(fill=X)
     
        self.refreshList()
        mainloop()


    def refreshList(self):
        self.SubjectList.delete(0,END)
        self.Subjects = self.database_teleport.GetAllSubjects()
        for each_fucking_subject in self.Subjects:
            self.SubjectList.insert(END,each_fucking_subject[0])

    def addSubj(self):
        self.add_window = Tk()
        self.add_window.configure(bg=framecolor)
        self.add_window.title(u"Добавить дисциплину")
        
        self.text = Label(self.add_window, text = u"Введите название дисциплины:", bg=framecolor, height=2, font=headerfont)
        self.text.pack(fill=X)
        self.SubjEntry = Entry(self.add_window, bg=framecolor, font=headerfont)
        self.SubjEntry.insert(0, u"Название")
        self.SubjEntry.pack(fill=X)

        Button( self.add_window, 
                text = u'Готово',
                command = self.confirmCreation,
				height=2,
				bg=greenbutton,
				font=buttonfont, 
				highlightbackground="#B0C4DE",
				activebackground=nicecolor).pack(fill=X)

        mainloop()

    def confirmCreation(self):
        subjName = self.SubjEntry.get()
        self.database_teleport.AddSubject(subjName)
        self.add_window.destroy()
        self.refreshList()

    def delSubj(self):
        num = self.SubjectList.curselection()
        self.database_teleport.DeleteSubject(self.Subjects[int(num[0])][0])
        self.refreshList()

    def nextWindow(self):
        num = self.SubjectList.curselection()
        subject_name=self.Subjects[int(num[0])][0]
        self.master.destroy()
        nw = SchoolGUI(subject_name)

class SchoolGUI:
    def __init__(self, subject_name):
        self.subject_name = subject_name
        self.master = Tk()
        height=600
        width=660
        ws=self.master.winfo_screenwidth()
	hs=self.master.winfo_screenheight()
        x = (ws/2) - (width/2)
        y = (hs/2) - (height/2)
        self.master.geometry('%dx%d+%d+%d' % (width, height, x, y))
        self.master.configure(bg=framecolor)
        self.master.title(u"Школы")
        self.database_teleport = cdb.Countrydb()
        Label(self.master, text="Шаг 2", bg=framecolor, font=headerfont).grid(column=0, row=0, columnspan=2, sticky=W+E)
        Label(  self.master,
                text = u"Добавьте или удалите школы:",
				bg=framecolor,
				font=headerfont).grid( column=0, row=1, columnspan=2, sticky=W+E)

        self.SchoolNamesListbox = Listbox(self.master, font=headerfont, width=25, selectbackground=nicecolor)
        self.SchoolNamesListbox.grid(row=2, column=0, sticky=W)

        self.SchoolHoursListbox = Listbox(self.master,font=headerfont, width=25, selectbackground=nicecolor)
        self.SchoolHoursListbox.grid(row=2, column=1, sticky=W)

        Button( self.master, 
                text=u'Добавить',
                command=self.addSchool,
				height=2,
				bg=redbutton,
				font=buttonfont, 
				highlightbackground="#B0C4DE",
				activebackground=nicecolor).grid(row = 3, column=0, sticky=W+E)

        Button( self.master, 
                text=u'Удалить',
                command=self.delSchool,
				height=2,
				bg=redbutton,
				font=buttonfont, 
				highlightbackground="#B0C4DE",
				activebackground=nicecolor).grid(row = 3, column=1, sticky=W+E)

        Button( self.master, 
                text=u'Далее к Шагу 3: Добавление учителей ->',
                command=self.nextWindow,
				height=3,
				bg=greenbutton,
				font=buttonfont, 
				highlightbackground="#B0C4DE",
				activebackground=nicecolor).grid(row = 4, column=0, columnspan=2, sticky=W+E)
        

        self.refreshLists()
        mainloop()

    def refreshLists(self):
        self.SchoolNamesListbox.delete(0,END)
        self.SchoolHoursListbox.delete(0,END)

        self.Schools = self.database_teleport.GetSchools(self.subject_name)
        for each_fucking_school in self.Schools:
            self.SchoolNamesListbox.insert(END,each_fucking_school[0])
            self.SchoolHoursListbox.insert(END,each_fucking_school[1])

    def addSchool(self):
        self.add_window = Tk()
        self.add_window.geometry("450x250")
        self.add_window.configure(bg=framecolor)
        self.add_window.title(u"Добавить школу")

        self.text = Label(self.add_window, text=u"Введите название школы и \nтребуемое количество часов \n в рамках дисциплины:", bg=framecolor, width=30, height=3, font=headerfont)
        self.text.pack()
        self.NameEntry = Entry( self.add_window, bg=framecolor, width=30, font=headerfont)
        self.NameEntry.insert(0,u'Название')
        self.NameEntry.pack()

        self.HoursEntry = Entry(    self.add_window, bg=framecolor, width=30, font=headerfont)
        self.HoursEntry.insert(0,u'Часы')
        self.HoursEntry.pack()

        Button( self.add_window, 
                text = u'Перейти к Шагу 3: Добавление учителей ->',
                command = self.confirmCreation,
				height=2,
				bg=greenbutton,
				font=buttonfont, 
				highlightbackground="#B0C4DE",
				activebackground=nicecolor).pack(fill=X)

        mainloop()

    def confirmCreation(self):
        schName = self.NameEntry.get()
        hours = self.HoursEntry.get()
        self.database_teleport.AddSchool(schName, self.subject_name, hours)
        self.add_window.destroy()
        self.refreshLists()


    def delSchool(self):
        num = self.SchoolNamesListbox.curselection()
        self.database_teleport.DeleteSchool(self.Schools[int(num[0])][0],self.subject_name)
        self.refreshLists()

    def nextWindow(self):
        subject_name = self.subject_name
        self.master.destroy()
        nw = TeachGUI(subject_name)

class TeachGUI:
    def __init__(self, subject_name):
        self.master = Tk()
        height=600
        width=660
        ws=self.master.winfo_screenwidth()
	hs=self.master.winfo_screenheight()
        x = (ws/2) - (width/2)
        y = (hs/2) - (height/2)
        self.master.geometry('%dx%d+%d+%d' % (width, height, x, y))
        self.master.configure(bg=framecolor)
        self.master.title(u"Учителя")
        self.subject_name = subject_name
        self.database_teleport = cdb.Countrydb()
        Label(self.master, text=u"Шаг 3", bg=framecolor, font=headerfont).grid(column=0, row=0, columnspan=2, sticky=W+E)
        Label(  self.master,
                text = u"Добавьте или удалите учителей",
				bg=framecolor,
				font=headerfont).grid( column=0, row=1, columnspan=2, sticky=W+E)

        self.TeacherNamesListbox = Listbox(self.master, width=25, font=headerfont, selectbackground=nicecolor)
        self.TeacherNamesListbox.grid(row=2, column=0, sticky=W)

        self.TeacherHoursListbox = Listbox(self.master,width=25, font=headerfont, selectbackground=nicecolor)
        self.TeacherHoursListbox.grid(row=2, column=1, sticky=W)

        Button( self.master, 
                text=u'Добавить',
                command=self.addTeacher,
				height=2,
				bg=redbutton,
				font=buttonfont, 
				highlightbackground="#B0C4DE",
				activebackground=nicecolor).grid(row = 3, column=0, sticky=W+E)

        Button( self.master, 
                text=u'Удалить',
                command=self.delTeacher,
				height=2,
				bg=redbutton,
				font=buttonfont, 
				highlightbackground="#B0C4DE",
				activebackground=nicecolor).grid(row = 3, column=1, sticky=W+E)

        Button( self.master, 
                text=u'Переход к Шагу 4: Просмотр результатов ->',
                command=self.nextWindow,
				height=3,
				bg=greenbutton,
				font=buttonfont, 
				highlightbackground="#B0C4DE",
				activebackground=nicecolor).grid(row = 4, column=0, columnspan=2, sticky=W+E)
        

        self.refreshLists()
        mainloop()

    def refreshLists(self):
        self.TeacherNamesListbox.delete(0,END)
        self.TeacherHoursListbox.delete(0,END)

        self.Teachers = self.database_teleport.GetTeachers(self.subject_name)
        for each_fucking_teacher in self.Teachers:
            self.TeacherNamesListbox.insert(END,each_fucking_teacher[0])
            self.TeacherHoursListbox.insert(END,each_fucking_teacher[1])

    def addTeacher(self):
        self.add_window = Tk()
        self.add_window.configure(bg=framecolor)
        self.add_window.title(u"Добавить учителя")
 
        self.text = Label(self.add_window, text=u"Введите ФИО учителя и\nколичество нераспределённых\nчасов у него: ", bg=framecolor, width=30, height=3, font=headerfont)
        self.text.grid(row=0, column=0, columnspan=2)
        self.NameEntry = Entry( self.add_window, bg=framecolor, width=25, font=headerfont)
        self.NameEntry.insert(0,u'Имя и фамилия')
        self.NameEntry.grid(row = 1, column = 0, columnspan=2)

        self.HoursEntry = Entry(    self.add_window, bg=framecolor, width=25, font=headerfont)
        self.HoursEntry.insert(0,u'Часы')
        self.HoursEntry.grid(row = 2, column = 0, columnspan=2)
        """
        Label(self.add_window, text = u'Укажите расстояние до школ:', bg=framecolor, width=30, font=mainfont)

        schoolslist = database_teleport.GetSchools(self.subject_name)
        for i in range(0, database_teleport.GetSchools(self.subject_name)):
            Label(self.master, text = , bg=framecolor)

        """
        Button( self.add_window, 
                text = u'Готово',
                command = self.confirmCreation,
				width=35,
				height=2,
				bg=greenbutton,
				font=buttonfont, 
				highlightbackground="#B0C4DE",
				activebackground=nicecolor).grid(row = 3, column = 0, columnspan = 2)

        mainloop()

    def confirmCreation(self):
        tName = self.NameEntry.get()
        hours = self.HoursEntry.get()
        self.database_teleport.AddTeacher(tName, self.subject_name, hours)
        self.add_window.destroy()
        self.refreshLists()


    def delTeacher(self):
        num = self.TeacherNamesListbox.curselection()
        self.database_teleport.DeleteTeacher(self.Teachers[int(num[0])][0],self.subject_name)
        self.refreshLists()

    def nextWindow(self):
        
        subject_name = self.subject_name
        if self.database_teleport.CheckIfDistanceNeeded(subject_name):
            self.master.destroy()
            nw = TeachDistanceGUI(subject_name)
        else:
            self.master.destroy()
            nw = ResultGUI(subject_name)

        
class TeachDistanceGUI:
    def __init__(self, subject_name):
        self.master = Tk()
        self.master.configure(bg=framecolor)
        self.master.title(u"Расстояния до школ")
        self.subject_name = subject_name
        self.database_teleport = cdb.Countrydb()
        Label(  self.master,
                text = u"Укажите расстояния учителей от школ",
				bg=framecolor,
				font=headerfont).grid( column=0, row=0, columnspan=3, sticky=W+E)

        TeachersList = self.database_teleport.GetTeachers(self.subject_name)
        # Array where |TeacherName|Label with school name|Entry with distance are stored
        self.ElementsArray = []
        
        row_counter = 1
        elements_counter = 0
        for i in range(0, len(TeachersList)):

            SchoolsList = self.database_teleport.GetNoDistanceSchools(self.subject_name, TeachersList[i][0])
            if SchoolsList != []:
                Label(self.master, text = TeachersList[i][0]+u":", bg=framecolor, font=headerfont).grid(row = row_counter, column = 0)
                row_counter+=1
                for j in range(0, len(SchoolsList)):
                    self.ElementsArray.append([TeachersList[i][0], SchoolsList[j][0] ,Label(self.master,text = SchoolsList[j][0]+u":", bg=framecolor, font=headerfont), Entry(self.master, bg=framecolor, font=headerfont)])
                    self.ElementsArray[elements_counter][2].grid(row = row_counter, column = 1)
                    self.ElementsArray[elements_counter][3].grid(row = row_counter, column = 2)
                    row_counter+=1
                    elements_counter+=1 
        
        Button( self.master, 
                text=u'Далее',
                command=self.confirmAction,
				bg=greenbutton,
				height=2,
				font=buttonfont, 
				highlightbackground="#B0C4DE",
				activebackground=nicecolor).grid(row = row_counter, column=0, columnspan=3, sticky=W+E)
        
        mainloop()

    def confirmAction(self):
        subject_name = self.subject_name
        for element in self.ElementsArray:
            #print(subject_name, element[1], element[0], float(element[3].get()))
            self.database_teleport.AddDistance(subject_name, element[1], element[0], float(element[3].get()))
        self.master.destroy()
        nw = ResultGUI(subject_name)

class ResultGUI:
    def __init__(self, subject_name):
        self.master = Tk()
        height=435
        width=650
        ws=self.master.winfo_screenwidth()
	hs=self.master.winfo_screenheight()
        x = (ws/2) - (width/2)
        y = (hs/2) - (height/2)
        self.master.geometry('%dx%d+%d+%d' % (width, height, x, y))
        self.master.title(u"Результат")
        self.master.configure(bg=framecolor)
        self.database_teleport = cdb.Countrydb()
        Distance, Demand, Demand_Names, Supply, Supply_Names = self.database_teleport.ExportToMatrix(subject_name)

        A = tt.transport(Supply, Demand, Distance)
        print("*"*10)
        print(A[7])
        print("*"*10)
        print(A[0])

        Label(self.master, text = u"Результат распределения учителей по школам:", height=2, bg=framecolor, font=headerfont).grid(row = 0, column = 0, columnspan = 2, sticky=W)
        rows = 1
        for i in range(0, len(Supply_Names)):
            Label(self.master, text = Supply_Names[i], bg=schoolcolor, font=headerfont).grid(row=rows, column = 0, columnspan=2, sticky=W+E)
            rows+=1
            for j in range(0, len(Demand_Names)):
                if (str(A[0][i][j])<>"nan") and (A[0][i][j] <> 0):
                    Label(self.master, text = Demand_Names[j]+u": "+str(A[0][i][j]).decode("utf8")+u" часов", bg=framecolor, font=headerfont).grid(row = rows, column=1, sticky=W)
                    rows+=1
            if A[7] == 1:
                unused = A[0][i][len(Demand_Names)]
                if (str(unused) <> "nan") and (unused <> 0):
                    Label(self.master, text =u"Не использовано: "+str(unused).decode("utf-8")+u" часов", bg=framecolor, font=headerfont).grid(row=rows, column=1, sticky=W)
                    rows+=1
        if A[7] == 2:
            for j in range(0, len(Demand_Names)):
                unused = A[0][len(Supply_Names)][j]
                if (str(unused) <> "nan") and (unused <> 0):
                    Label(self.master, text =u"В школе "+ Demand_Names[j] + u" не использовано: "+str(unused).decode("utf-8")+u" часов", bg=framecolor, font=headerfont).grid(row=rows, column=0, columnspan=2, sticky=W)
                    rows+=1

            
        
        rows+=1        
        Button(self.master, text = u"Выход", command=exit, width=27, height=3, bg=greenbutton, font=buttonfont, highlightbackground="#B0C4DE", activebackground=nicecolor).grid(row = rows, column=1, sticky=W)
        Button(self.master, text = u"Заново", command=self.restart, width=27, height=3, bg=redbutton, font=buttonfont, highlightbackground="#B0C4DE", activebackground=nicecolor).grid(row = rows, column=0, sticky=W)
        mainloop()

    def restart(self):
        self.master.destroy()
        nw = SpecGUI()


if __name__ == "__main__":
     window = GreetingWindow()


