# -*- coding: utf-8 -*-
import sqlite3
import subprocess
import os
import os.path
import numpy as np


__name__ = "countrydb"

class Countrydb:
    country_folder ="db/" # DO NOT CHANGE THIS PATH - BASH SCRIPT USES IT!
    def __init__(self):	
        if (os.path.isfile(self.country_folder+"country.db")) == False:
            subprocess.call(["mkdir",self.country_folder])
            subprocess.call(["touch",self.country_folder+"country.db"])
            subprocess.call(["sh","read.sh"])


    def GetAllSubjects(self):
        con = sqlite3.connect(self.country_folder+"country.db")
        cur = con.cursor()
        
        answer = cur.execute("SELECT sName FROM Subjects").fetchall()

        con.commit()
        cur.close()

        return answer

    def AddSubject(self,Name):
        con = sqlite3.connect(self.country_folder+"country.db")
        cur = con.cursor()
        
        cur.execute("INSERT INTO Subjects VALUES(?)",(Name,))

        con.commit()
        cur.close()

    def DeleteSubject(self, Name):
        con = sqlite3.connect(self.country_folder+"country.db")
        cur = con.cursor()
        
        cur.execute("DELETE FROM Subjects WHERE sName = ?",(Name,))
        cur.execute("DELETE FROM Schools WHERE sName = ?",(Name,))
        cur.execute("DELETE FROM Teachers WHERE sName = ?",(Name,))
        cur.execute("DELETE FROM Distance WHERE sName = ?",(Name,))

        con.commit()
        cur.close()

    def GetSchools(self,Subject, order = None):
        con = sqlite3.connect(self.country_folder+"country.db")
        cur = con.cursor()
        if order == None:
            answer = cur.execute("SELECT schName, schHours FROM Schools WHERE sName = ?",(Subject,)).fetchall()
        else:
            answer = cur.execute("SELECT schName, schHours FROM Schools WHERE sName = ? ORDER BY schName ASC",(Subject,)).fetchall()


        con.commit()
        cur.close()
        
        return answer
       
    def GetNoDistanceSchools(self, Subject, Teacher_name):
        con = sqlite3.connect(self.country_folder+"country.db")
        cur = con.cursor()
        
        answer = cur.execute("SELECT schName FROM Schools WHERE sName = ? EXCEPT SELECT schName FROM Distance WHERE tName = ? AND sName = ?",(Subject, Teacher_name, Subject)).fetchall()

        con.commit()
        cur.close()

        return answer


    def AddSchool(self, Name, Subject, Hours):
        con = sqlite3.connect(self.country_folder+"country.db")
        cur = con.cursor()

        cur.execute("INSERT INTO Schools VALUES(?,?,?)",(Name,Subject,Hours))

        con.commit()
        cur.close()
   
    def DeleteSchool(self, Name, Subject):
        con = sqlite3.connect(self.country_folder+"country.db")
        cur = con.cursor()

        cur.execute("DELETE FROM Schools WHERE schName = ? and sName = ?",(Name,Subject))
        cur.execute("DELETE FROM Distance WHERE schName = ? and sName = ?",(Name,Subject))

        con.commit()
        cur.close()

    def GetTeachers(self,Subject, order=None):
        con = sqlite3.connect(self.country_folder+"country.db")
        cur = con.cursor()
        
        if order == None:
            answer = cur.execute("SELECT tName, tHours FROM Teachers WHERE sName = ?",(Subject,)).fetchall()
        else:
            answer = cur.execute("SELECT tName, tHours FROM Teachers WHERE sName = ? ORDER BY tName ASC",(Subject,)).fetchall()

        con.commit()
        cur.close()
        return answer

    def AddTeacher(self, Name, Subject, Hours):
        con = sqlite3.connect(self.country_folder+"country.db")
        cur = con.cursor()

        cur.execute("INSERT INTO Teachers VALUES(?,?,?)",(Name, Subject, Hours))
        
        con.commit()
        cur.close()

    def DeleteTeacher(self, Name, Subject):
        con = sqlite3.connect(self.country_folder+"country.db")
        cur = con.cursor()

        cur.execute("DELETE FROM Teachers WHERE tName = ? and sName = ?",(Name, Subject))
        cur.execute("DELETE FROM Distance WHERE tName = ? and sName = ?",(Name, Subject))

        con.commit()
        cur.close()

    def AddDistance(self, Subject_Name, School_Name, Teacher_Name,  Distance):
        con = sqlite3.connect(self.country_folder+"country.db")
        cur = con.cursor()
        
        cur.execute("INSERT INTO Distance VALUES(?,?,?,?)",(School_Name, Teacher_Name, Distance, Subject_Name))

        con.commit()
        cur.close()

    def GetDistance(self, Subject_Name, Teacher_Name):
        con = sqlite3.connect(self.country_folder+"country.db")
        cur = con.cursor()

        answer = cur.execute("SELECT schName, distance FROM Distance WHERE sName = ? and tName = ? ORDER BY schName ASC",(Subject_Name, Teacher_Name)).fetchall()

        con.commit()
        cur.close()

        return answer

    def CheckIfDistanceNeeded(self, Subject_Name):
        con = sqlite3.connect(self.country_folder+"country.db")
        cur = con.cursor()

        answer = cur.execute("SELECT schName, tName FROM Schools S, Teachers T WHERE S.sName = ? and T.sName = ? EXCEPT SELECT schName, tName FROM Distance WHERE sName = ?",(Subject_Name, Subject_Name, Subject_Name)).fetchall()
        con.commit()
        cur.close()
        if answer == []:
            return False
        else:
            return True        

    def ExportToMatrix(self, Subject):
        Teachers = self.GetTeachers(Subject, order="yes, please")
        Schools = self.GetSchools(Subject, order="yes, please")
         
        Distance = np.array([])
        Demand = np.array([])
        Supply = np.array([])
        Demand_names = np.array([])
        Supply_names = np.array([])
       
        # exporting demand
        for school in Schools:
            Demand = np.append(Demand, float(school[1]))
            Demand_names =  np.append(Demand_names, school[0])
 

        # exporting distance and supply
        for teacher in Teachers:
            Supply = np.append(Supply, float(teacher[1]))
            Supply_names = np.append(Supply_names, teacher[0])
            dist = self.GetDistance(Subject, teacher[0])
            for i in range(0, len(dist)):
                assert Schools[i][0] == dist[i][0]
                Distance = np.append(Distance, float(dist[i][1]))

        return Distance.reshape(len(Teachers), len(Schools)), Demand, Demand_names, Supply, Supply_names

    
         

def Test():
    A = Countrydb()
#    print(A.GetAllSubjects())
#    print(A.GetSchools("Math"))
#    print(len(A.GetSchools("Math")))
#    print(A.GetNoDistanceSchools('Subject_One','Teacher_Three')[0][0])
#    print(A.GetTeachers('Subject_One')[0][0])
#    print(A.ExportToMatrix('Subject_One'))
#    exit()   

    A.AddSubject('Subject_One')
    A.AddSchool('School_One','Subject_One',100)
    A.AddSchool('School_Two','Subject_One',200)
    A.AddSchool('School_Three','Subject_One',300)

    A.AddTeacher('Teacher_One','Subject_One',110)
    A.AddTeacher('Teacher_Two','Subject_One',220)
    A.AddTeacher('Teacher_Three','Subject_One',330)

    A.AddDistance('Subject_One','School_One','Teacher_One',1.1)
    A.AddDistance('Subject_One','School_One','Teacher_Two',2.1)
    A.AddDistance('Subject_One','School_One','Teacher_Three',3.1)

    A.AddDistance('Subject_One','School_Two','Teacher_One',1.2)
    A.AddDistance('Subject_One','School_Two','Teacher_Two',2.2)
    A.AddDistance('Subject_One','School_Two','Teacher_Three',3.2)

    A.AddDistance('Subject_One','School_Three','Teacher_One',1.3)
    A.AddDistance('Subject_One','School_Three','Teacher_Two',2.3)
    A.AddDistance('Subject_One','School_Three','Teacher_Three',3.3)
    print(A.ExportToMatrix('Subject_One'))

    """
    A.AddDistance('Subject_One','School_One','Teacher_One',10)
    A.AddDistance('Subject_One','School_One','Teacher_Two',11)
    A.AddDistance('Subject_One','School_One','Teacher_Three',12)

    A.AddDistance('Subject_One','School_Two','Teacher_One',10)
    A.AddDistance('Subject_One','School_Two','Teacher_Two',11)

    A.AddDistance('Subject_One','School_Three','Teacher_One',8)
    A.AddDistance('Subject_One','School_Three','Teacher_Three',4)
    print(A.GetNoDistanceSchools('Subject_One','Teacher_Three'))
    exit()
    
    A.AddSubject('Жижа'.decode('utf8'))
    A.AddSubject('Мокрошка'.decode('utf8'))
    A.AddSubject('Печень'.decode('utf8'))
    A.DeleteSubject('Жижа'.decode('utf8'))

    A.AddSchool("45",u'Мокрошка',10)
    A.AddSchool("45",u'Печень',7)
    A.AddSchool(u"школа 47",u'Мокрошка',8)

    A.AddTeacher(u'Марьиванна',u'Печень',3)
    A.AddTeacher(u'Петровна',u'Мокрошка',18)
    A.AddTeacher(u'Дырокол',u'Печень',4)
    
    A.AddDistance("45",u'Петровна',10)

    A.DeleteTeacher(u'Дырокол',u'Печень')
    A.DeleteSchool("45",u'Мокрошка')

    #A.DeleteSubject(u'Печень')
    #for DICK in A.GetAllSubjects():
    #    print DICK[0].encode('utf8')
    print(A.GetAllSubjects())
    print(A.GetSchools(u'Мокрошка'))
    print(A.GetTeachers(u'Печень'))
    """

