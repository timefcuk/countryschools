University venture 

STEPS:

* create db class (DONE)
* create interface class (DONE)
* integrate db in the interface (DONE)
* create function class (DONE)
* integrate db in the function class (DONE)
* integrate function class in the interface (DONE)

TODO:

1. Add new window with info about every step of the 
    * Step 1: Choose or add a discipline
    * Step 2: Add or delete schools 
    * Step 3: Add or delete teachers
    * Step 4: Add missing info about distances
    * Step 5: Get result
2. Add each step's name and number to it's window
3. Center values of listboxes 
    * right now they are positioned on the left of each listbox vidget 
4. Add hours to every hour 
    * i.e. was "40.0" -> "40.0 HOURS"



## **HOW TO LAUNCH?** ##

1. Install Tkinter (`tk` in arch, `python-tk` in ubuntu)
2. Install `virtualenv`
3. `git clone https://timefcuk@bitbucket.org/timefcuk/countryschools.git`
4. `virtualenv countryschools -p python2.7`
5. `cd countryschools`
6. `source bin/activate`
7. `pip install -r requirements.txt`
8. `python countrygui.py`