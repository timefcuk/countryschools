PRAGMA foreign_keys=ON;
PRAGMA encoding = "UTF-8"; 
BEGIN TRANSACTION;
CREATE TABLE Subjects(sName text primary key);
CREATE TABLE Schools(schName text, sName text, schHours float not null, primary key(SchName, sName), foreign key (sName) references Subjects(sName));
CREATE TABLE Teachers(tName text, sName text, tHours float not null, primary key(tName, sName), foreign key (sName) references Subjects(sName));
CREATE TABLE Distance(schName text references Schools(schName), tName text references Teachers(tName), Distance float not null, sName text references Subjects(sName), primary key(schName, tName, sName));
COMMIT;
